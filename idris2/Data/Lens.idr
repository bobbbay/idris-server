module Data.Lens

import public Data.Product

--     ┌──────┐
-- s >─┤      ├─> a
--     │      │
-- t <─┤      ├─< b
--     └──────┘
public export
record Lens (a, b, s, t : Type) where
  constructor MkLens
  get : s -> a
  set : s * b -> t

--     ┌──────┐       ┌──────┐
-- x >─┤      ├─> s >─┤      ├─> a
--     │  l2  │       │  l1  │
-- y <─┤      ├─< t <─┤      ├─< b
--     └──────┘       └──────┘
export
compose : (l1 : Lens a b s t) -> (l2 : Lens s t x y) -> Lens a b x y
compose (MkLens g1 s1) (MkLens g2 s2) = 
  MkLens (\z => g1 (g2 z)) 
         (\z => s2 (fst z && s1 (g2 (fst z) && snd z)))
