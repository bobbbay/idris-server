module Data.Sum

prefix 6 <+, +> 

public export
data (+) : Type -> Type -> Type where
  (<+) : a -> a + b 
  (+>) : b -> a + b

public export
choice : (a -> c) -> (b -> c) -> a + b -> c
choice f g (<+ x) = f x
choice f g (+> x) = g x

public export
elim : (a -> c) -> (b -> d) -> a + b -> c + d
elim f g (<+ x) = <+ (f x)
elim f g (+> x) = +> (g x)

public export
Bifunctor (+) where
  bimap = elim
