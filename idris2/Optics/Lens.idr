module Optics.Lens

import public Optics.FreePara

public export
Lens : (a, b, s, t : Type) -> Type
Lens = ParaList [] []
