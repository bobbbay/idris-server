module Optics.Para

import Data.List1
import Data.List
import Data.Product
import Data.Sum
import Data.Lens

%hide Prelude.Basics.(&&)
%hide DPair.(.fst)
%hide DPair.(.snd)

K : b -> a -> b
K = const

Para : (p, q, a, b, s, t : Type) -> Type
Para p q a b s t = Lens a b (s * p) (q * t)

seq : Para p q a b s t -> Para p' q' s t x y -> Para (p * p') (q * q') a b x y
seq ((MkLens get1 set1)) ((MkLens get2 set2)) = 
    MkLens (\(x && (p && p')) => get1 (get2 (x && p') && p))
           (\((x && (p && p')) && b) => let (q && t) = set1 ((get2 (x && p') && p) && b)
                                            (q' && y) = set2 ((x && p') && t)
                                         in (q && q') && y)
 
gigaTensor : Para p q a b s t 
          -> Para p' q' x y z w 
          -> Para (p * p') (q * q') (a * x) (b * y) (s * z) (t * w)
gigaTensor (MkLens get1 set1) (MkLens get2 set2) = 
  MkLens (bimap get1 get2 . shuffle) 
         (shuffle 
         . bimap set1 set2 
         . fork (bimap p1 p1) 
                (bimap p2 p2) 
         . mapFst shuffle)

reparam : Para p q a b s t
       -> Lens p q p' q'
       -> Para p' q' a b s t
reparam (MkLens get1 set1) (MkLens get2 set2) = 
  MkLens (get1 . mapSnd get2) 
         (fork (set2 . bimap (p2 . p1) 
                             p1)
               (p2 . p2) 
         . mapSnd (set1 . mapFst (mapSnd get2)) 
         . Product.dup)

tensor : Monoid q => Para p q a b s t 
                  -> Para p q x y z w 
                  -> Para p q (a * x) (b * y) (s * z) (t * w)
tensor l1 l2 =
  reparam (gigaTensor l1 l2)
          (MkLens Product.dup (Product.uncurry (<+>) . p2))
