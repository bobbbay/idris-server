module Optics.Dependent

import Data.Product
import Data.Sum

public export
record DepParLens (p : Type) (q : p -> Type) 
                  (a : Type) (b : a -> Type) 
                  (s : Type) (t : s -> Type)  where
  constructor MkDepLens
  get : p * s -> a
  set : (pv : p) -> (es : s) -> b (get (pv && es)) -> q pv * t es

setRes : forall a, p, p', x, s 
      . {0 b : a -> Type}
      -> {0 q : p -> Type}
      -> {0 t : s -> Type}
      -> {0 q' : p' -> Type}
      -> {0 y : x -> Type} 
      -> (get1 : p * s -> a)
      -> (set1 : (pv : p) -> (es : s) -> b (get1 (pv && es)) -> q pv * t es)
      -> (get2 : p' * x -> s)
      -> (set2 : (pv : p') -> (es : x) -> t (get2 (pv && es)) -> q' pv * y es)
      -> (pv : p * p') 
      -> (es : x) 
      -> b (get1 (p1 pv && get2 (p2 pv && es)))
      -> (q (p1 pv) * q' (p2 pv)) * y es
setRes get1 set1 get2 set2 pv es z = 
  let (res && cont) = set1 (p1 pv) (get2 (p2 pv && es)) z 
      (r1 && r2) = set2 (p2 pv) es (cont) in (res && r1) && r2

||| Sequential composition of dependent lenses
public export
compose : {0 b : a -> Type}
    -> {0 q : p -> Type}
    -> {0 t : s -> Type}
    -> {0 q' : p' -> Type}
    -> {0 y : x -> Type} 
    -> DepParLens p q a b s t 
    -> DepParLens p' q' s t x y 
    -> DepParLens (p * p') (\pv => q (p1 pv) * q' (p2 pv))
                  a b
                  x y
compose (MkDepLens get1 set1) (MkDepLens get2 set2) = 
  MkDepLens (\pv => get1 ((p1 (p1 pv)) && get2 ((p2 (p1 pv)) && (p2 pv))))
            (let v = Dependent.setRes {b} get1 set1 get2 set2 in 
                 \pv, x, bee => v pv x (rewrite sym $ proj1Pair pv x in 
                                       replace {p = \k => b (get1 (p1 (p1 (pv && x)) && get2 (p2 (p1 (pv && x)) && k)))} 
                                               (proj2Pair pv x) bee))

distribute : a * (b + c) -> a * b + a * c
distribute (fst && (+> x)) = +> (fst && x)
distribute (fst && (<+ x)) = <+ (fst && x)

extRes : forall p
    . {0 q : p -> Type}
   -> {0 t : s -> Type}
   -> {0 q' : p -> Type}
   -> {0 y : x -> Type} 
   -> (get1 : p * s -> a)
   -> (set1 : (pv : p) -> (es : s) -> b (get1 (pv && es)) -> q pv * t es)
   -> (get2 : p * x -> c)
   -> (set2 : (pv : p) -> (es : x) -> d (get2 (pv && es)) -> q' pv * y es)
   -> (pv : p) -> (es : s + x) -> Sum.choice b d (Sum.elim get1 get2 (Dependent.distribute (pv && es))) -> (q pv + q' pv) * Sum.choice t y es
extRes get1 set1 get2 set2 pv (+> w) z = 
  mapFst (+>) (set2 pv w z)
extRes get1 set1 get2 set2 pv (<+ w) z = 
  mapFst (<+) $ set1 pv w z

||| External choice operator. Takes two lenses and allows you to pick which one to execute
public export
extChoice : forall a, p, p', x
     . {0 b : a -> Type}
    -> {0 q : p -> Type}
    -> {0 t : s -> Type}
    -> {0 q' : p -> Type}
    -> {0 y : x -> Type} 
    -> DepParLens p q a b s t 
    -> DepParLens p q' c d x y
    -> DepParLens p (\st => q st + q' st)
                  (a + c) (Sum.choice b d) 
                  (s + x) (Sum.choice t y)
extChoice (MkDepLens get1 set1) (MkDepLens get2 set2) = 
  MkDepLens (Sum.elim get1 get2 . Dependent.distribute) 
            (extRes get1 set1 get2 set2)

