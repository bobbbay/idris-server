||| Para lenses but the state parameter is a list of types instead of a product
module Optics.FreePara

import Data.List1
import Data.List
import Data.Product
import Data.Sum
import Data.ListType

--         p    q        
--         │    │        
--     ┌───┴────┴───┐
-- s >─┤            ├─> a
--     │            │
-- t <─┤            ├─< b
--     └────────────┘
public export
record ParaList (p, q : List Type) (a, b, s, t : Type) where
  constructor MkPara
  get : FromList1 (s ::: p) -> a
  set : FromList1 (s ::: p) * b -> FromList1 (t ::: q)


pre : {p, p' : _} -> FromList1 (a ::: (p ++ p')) -> FromList1 (a ::: p)
pre x {p = []} = listHead x
pre (fst && snd) {p = (y :: xs)} = fst && pre snd

-- Swap the head of a FromList1 with the given value
chHead : {p : _} -> b -> FromList1 (a ::: p) ->  FromList1 (b ::: p)
chHead ty x {p = []} = ty
chHead ty (fst && snd) {p = (y :: xs)} = ty && snd

post : {p, p' : _} -> FromList1 (a ::: (p ++ p')) -> FromList1 (a ::: p')
post x {p = []} = x
post (x && y) {p = (z :: zs)} = chHead x (post y)


concat' : {a, b : _} -> FromList1 (x ::: a) -> FromList1 (y ::: b) -> FromList1 (y ::: (a ++ b))
concat' z w = flipTail $ FromList1.concat w z


-- Sequential composition of paralenses with lists as states
export
compose : {p, p', q, q' : _} -> 
       ParaList p  q a b s t -> 
       ParaList p' q' s t x y -> 
       ParaList (p ++ p') (q ++ q') a b x y
compose (MkPara get1 set1) (MkPara get2 set2) = 
  MkPara (get1 . p2 . swap2 . fork (get2 . post) pre)
         ((\x => FreePara.concat' (chHead (listHead x.fst) (x.snd)) 
                              (x.fst))
         . fork (set2 . bimap (post . (.fst) . (.snd)) listHead . swap)
                (set1 . (\x => mapFst (chHead x.fst . pre) x.snd) . (.snd))
         . mapFst (set1 . (\x => (mapFst (chHead x.fst . pre) x.snd)))
         . Product.dup
         . mapFst (get2 . post . (.fst))
         . Product.dup )

gigaTensor : {p, p' : _} 
          -> ParaList p q a b s t 
          -> ParaList p' q' x y z w 
          -> ParaList (p ++ p') (q ++ q') (a * x) (b * y) (s * z) (t * w)
gigaTensor (MkPara get1 set1) (MkPara get2 set2) = 
  MkPara (bimap get1 get2 . asPairs)
         (Product.uncurry toPairs 
         . bimap set1 set2 
         . shuffle
         . mapFst asPairs)

reparam : {p, p', q, q' : _} -> ParaList p q a b s t
       -> ParaList [] [] (FromList p) (FromList q) (FromList p') (FromList q')
       -> ParaList p' q' a b s t
reparam (MkPara get1 set1) (MkPara get2 set2) = 
  MkPara (get1 . mapTail get2)
         (\spb => let r = get2 (listTail (p1 spb)) 
                      s = set1 (mapFst (mapTail (const r)) spb) 
                      s2 = set2 (listTail (p1 spb) && listTail s)
                   in mapTail (const s2) s)

tensor : {p, q : _} 
      -> (combine : FromList q -> FromList q -> FromList q)
      -> ParaList p q a b s t 
      -> ParaList p q x y z w 
      -> ParaList p q (a * x) (b * y) (s * z) (t * w)
tensor c l1 l2 =
  reparam (gigaTensor l1 l2)
          (MkPara (Product.uncurry FromList.concat . Product.dup) 
                  (Product.uncurry c . FromList.split . p2))
{-
-}
