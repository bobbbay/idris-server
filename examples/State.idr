module State

import Server
import Server.EDSL.Lenses
import IOTTypes

InternalState : Type
InternalState = (Int, Double)

server : ServerTree InternalState
server = Prefix "path" $ Fork [ ResourceLens fstLens'
                              , ResourceLens sndLens']

main : IO ()
main = runServer Normal server (0, 3.14)
